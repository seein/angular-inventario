export interface Marca {
	id: number;
	nombre: string;
}

export interface Categoria {
	id: number;
	nombre: string;
}

export interface Proveedor {
	id: number;
	nombre: string;
}

export interface Producto {
	id: number;
	marca_id: number;
	categoria_id: number;
	proveedor_id: number;
	codigo: string;
	nombre: string;
	marca: string;
	categoria: string;
	proveedor: string;
	precio_proveedor: number;
	precio_venta: number;
	cantidad: number;
}
