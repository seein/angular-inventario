import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProveedoresService {
	url: string = 'http://localhost:8000';

	constructor(private http: HttpClient){ 

	}

	getAll(){
		return this.http.get(`${ this.url }/api/proveedores`);
	}

	agregarProveedor(nombre: string){
		return this.http.post(`${ this.url }/api/proveedores`, {nombre: nombre});
	}

	editarProveedor(id:number, nombre: string){
		return this.http.put(`${ this.url }/api/proveedores/${ id }`, {nombre: nombre});
	}

	eliminarProveedor(id: number){
		return this.http.delete(`${ this.url }/api/proveedores/${ id }`);
	}
}
