import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MarcasService {
	url: string = 'http://localhost:8000';

	constructor(private http: HttpClient){ 

	}

	getAll(){
		return this.http.get(`${ this.url }/api/marcas`);
	}

	agregarMarca(nombre: string){
		return this.http.post(`${ this.url }/api/marcas`, {nombre: nombre});
	}

	editarMarca(id:number, nombre: string){
		return this.http.put(`${ this.url }/api/marcas/${ id }`, {nombre: nombre});
	}

	eliminarMarca(id: number){
		return this.http.delete(`${ this.url }/api/marcas/${ id }`);
	}
}
