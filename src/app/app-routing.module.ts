import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { MarcaComponent } from './components/marca/marca.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { ProveedorComponent } from './components/proveedor/proveedor.component';
import { ProductoComponent } from './components/producto/producto.component';


const routes: Routes = [
	{ path: 'home', component: HomeComponent },
	{ path: 'marcas', component: MarcaComponent },
	{ path: 'categorias', component: CategoriaComponent },
	{ path: 'proveedores', component: ProveedorComponent },
	{ path: 'productos', component: ProductoComponent },
	{ path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
