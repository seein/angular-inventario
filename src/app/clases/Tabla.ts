import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

export class Tabla {
	dtOptions: DataTables.Settings = {};

	/* Utilizamos este desencadenante porque la búsqueda de la lista de marcas puede ser bastante larga,
      	Por lo tanto, aseguramos que los datos se obtienen antes de renderizar */
	dtTrigger: Subject<any> = new Subject();

	constructor(){
		this.dtOptions = {
      		pagingType: 'simple_numbers',
      		pageLength: 10,
      		lengthChange: false,
      		ordering: false,
      		language: {
      			"emptyTable":     "No hay datos disponibles en la tabla",
			    "info": 		  "Mostrando _START_ a _END_ de _TOTAL_ registros",
			    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
			    "infoFiltered":   "(filtrado de _MAX_ registros totales)",
			    "infoPostFix":    "",
			    "thousands":      ",",
			    "lengthMenu":     "Mostrando _MENU_ entradas",
			    "loadingRecords": "Cargando...",
			    "processing":     "Procesando...",
			    "search":         "Buscar:",
			    "zeroRecords":    "No se encontraron registros coincidentes",
			    "paginate": {
			        "first":      "Primera",
			        "last":       "Última",
			        "next":       "Siguiente",
			        "previous":   "Anterior"
			    }
	    	}
	    }
	}

	rerender(dtElement: DataTableDirective): void {
		dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		});
	}

	/*
	extractData(res) {
		const body = res.json();
		return body.data || {};
	} */
}