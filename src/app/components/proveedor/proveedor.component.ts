import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { ProveedoresService } from '../../services/proveedores.service';
import { Tabla } from '../../clases/Tabla';
import { Proveedor } from '../../interfaces/interfaces';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';

import Swal from 'sweetalert2';

// Para usar jQuery
declare var $: any;

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styles: []
})
export class ProveedorComponent implements OnInit, OnDestroy {
	@ViewChild(DataTableDirective, {static: false}) dtElement: DataTableDirective;

	proveedores: Proveedor[] = [];
	tabla = new Tabla();
	forma: FormGroup;
	accion: string;
	proveedorEditar: Proveedor;

	constructor(private _proveedoresService: ProveedoresService){ 
		this._proveedoresService.getAll().subscribe( (proveedores: Proveedor[]) => {
			this.proveedores = proveedores;
			this.tabla.dtTrigger.next();
		});
	}

	ngOnInit() {
		this.buildForm();
	}

	ngOnDestroy() {
	    // Do not forget to unsubscribe the event
	    this.tabla.dtTrigger.unsubscribe();
  	}

  	agregarProveedor(){
  		// Guardar datos 
  		var nombre = this.forma.value.nombre.toUpperCase();

  		this._proveedoresService.agregarProveedor(nombre).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Agregar a la lista de proveedores
  				this.proveedores.push({ id: resp.id, nombre: nombre });

  				// Ordenar proveedores por nombre en asc 
  				this.proveedores.sort((unProveedor, otroProveedor) => unProveedor.nombre.localeCompare(otroProveedor.nombre));

  				// Ocultar modal
  				$('#modalProveedor').modal('hide');

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `El proveedor ${ nombre } ha sido agregado correctamente.`, 'success');

  				// Reset formulario
  				this.resetForm();
  			} else {
  				// Ocultar modal
  				$('#modalProveedor').modal('hide');

  				// Mostrar alerta
  				Swal.fire('Oops!', `Ha ocurrido un error al agregar el proveedor ${ nombre }. Es posible que el proveedor ya exista.`, 'error');

  				// Reset formulario
  				this.resetForm();
  			}
  		});
  	}

  	editarProveedor(){
  		// Guardar datos
  		var nombre = this.forma.value.nombre.toUpperCase();

  		this._proveedoresService.editarProveedor(this.proveedorEditar.id, nombre).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Actualizar nombre en lista de proveedores 
  				const index = this.proveedores.findIndex(proveedor => proveedor.nombre == this.proveedorEditar.nombre);
  				this.proveedores[index].nombre = nombre;

  				// Ordenar proveedores por nombre en asc 
  				this.proveedores.sort((unProveedor, otroProveedor) => unProveedor.nombre.localeCompare(otroProveedor.nombre));

  				// Ocultar modal
  				$('#modalProveedor').modal('hide');

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `El proveedor ha sido editado correctamente.`, 'success');

  				// Reset formulario
  				this.resetForm();
  			} else {
  				// Ocultar modal
  				$('#modalProveedor').modal('hide');

  				// Mostrar alerta
  				Swal.fire('Oops!', `Ha ocurrido un error al editar el proveedor. Es posible que el nombre ya exista.`, 'error');

  				// Reset formulario
  				this.resetForm();
  			}
  		});
  	}

  	eliminarProveedor(proveedor: Proveedor, i: number){
  		this._proveedoresService.eliminarProveedor(proveedor.id).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Eliminar de la lista de proveedores
  				this.proveedores.splice(i, 1);

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `El proveedor ${ proveedor.nombre } ha sido eliminado correctamente.`, 'success');
  			} else {
  				Swal.fire('Oops!', `No es posible eliminar el proveedor ${ proveedor.nombre } mientras se encuentre asociado a un producto.`, 'error');
  			}
  		});
  	}

  	onSubmit(){
  		if(this.accion == 'agregar'){
  			this.agregarProveedor();
  		} else {
  			this.editarProveedor();
  		}
  	}

  	showModal(proveedor?: Proveedor){
  		if(proveedor){
  			this.accion = 'editar';
  			this.forma.controls['nombre'].setValue(proveedor.nombre);
  			this.proveedorEditar = proveedor; 
  		} else {
  			this.accion = 'agregar';
  		}

  		// Mostrar modal
  		$('#modalProveedor').modal('show');
  	}

  	private buildForm(){
		this.forma = new FormGroup({
			'nombre': new FormControl('', Validators.required)
		});
  	}

  	resetForm(){
  		this.forma.reset();
  	}
}
