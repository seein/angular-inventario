import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { CategoriasService } from '../../services/categorias.service';
import { Tabla } from '../../clases/Tabla';
import { Categoria } from '../../interfaces/interfaces';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';

import Swal from 'sweetalert2';

// Para usar jQuery
declare var $: any;

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styles: []
})
export class CategoriaComponent implements OnInit, OnDestroy {
	@ViewChild(DataTableDirective, {static: false}) dtElement: DataTableDirective;

	categorias: Categoria[] = [];
	tabla = new Tabla();
	forma: FormGroup;
	accion: string;
	categoriaEditar: Categoria;

	constructor(private _categoriasService: CategoriasService){ 
		this._categoriasService.getAll()
			.subscribe( (categorias: Categoria[]) => {
				this.categorias = categorias;
				this.tabla.dtTrigger.next();
			});
	}

	ngOnInit() {
		this.buildForm();
	}

	ngOnDestroy() {
	    // Do not forget to unsubscribe the event
	    this.tabla.dtTrigger.unsubscribe();
  	}

  	agregarCategoria(){
  		// Guardar datos 
  		var nombre = this.forma.value.nombre.toUpperCase();

  		this._categoriasService.agregarCategoria(nombre).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Agregar a la lista de categorias
  				this.categorias.push({ id: resp.id, nombre: nombre });

  				// Ordenar categorias por nombre en asc 
  				this.categorias.sort((unaCategoria, otraCategoria) => unaCategoria.nombre.localeCompare(otraCategoria.nombre));

  				// Ocultar modal
  				$('#modalCategoria').modal('hide');

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `La categoría ${ nombre } ha sido agregada correctamente.`, 'success');

  				// Reset formulario
  				this.resetForm();
  			} else {
  				// Ocultar modal
  				$('#modalCategoria').modal('hide');

  				// Mostrar alerta
  				Swal.fire('Oops!', `Ha ocurrido un error al agregar la categoría ${ nombre }. Es posible que la categoría ya exista.`, 'error');

  				// Reset formulario
  				this.resetForm();
  			}
  		});
  	}

  	editarCategoria(){
  		// Guardar datos
  		var nombre = this.forma.value.nombre.toUpperCase();

  		this._categoriasService.editarCategoria(this.categoriaEditar.id, nombre).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Actualizar nombre en lista de categorias 
  				const index = this.categorias.findIndex(categoria => categoria.nombre == this.categoriaEditar.nombre);
  				this.categorias[index].nombre = nombre;

  				// Ordenar categorias por nombre en asc 
  				this.categorias.sort((unaCategoria, otraCategoria) => unaCategoria.nombre.localeCompare(otraCategoria.nombre));

  				// Ocultar modal
  				$('#modalCategoria').modal('hide');

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `La categoría ha sido editada correctamente.`, 'success');

  				// Reset formulario
  				this.resetForm();
  			} else {
  				// Ocultar modal
  				$('#modalCategoria').modal('hide');

  				// Mostrar alerta
  				Swal.fire('Oops!', `Ha ocurrido un error al editar la categoría. Es posible que el nombre ya exista.`, 'error');

  				// Reset formulario
  				this.resetForm();
  			}
  		});
  	}

  	eliminarCategoria(categoria: Categoria, i: number){
  		this._categoriasService.eliminarCategoria(categoria.id).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Eliminar de la lista de categorias
  				this.categorias.splice(i, 1);

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `La categoría ${ categoria.nombre } ha sido eliminada correctamente.`, 'success');
  			} else {
  				Swal.fire('Oops!', `No es posible eliminar la categoría ${ categoria.nombre } mientras se encuentre asociada a un producto.`, 'error');
  			}
  		});
  	}

  	onSubmit(){
  		if(this.accion == 'agregar'){
  			this.agregarCategoria();
  		} else {
  			this.editarCategoria();
  		}
  	}

  	showModal(categoria?: Categoria){
  		if(categoria){
  			this.accion = 'editar';
  			this.forma.controls['nombre'].setValue(categoria.nombre);
  			this.categoriaEditar = categoria;
  		} else {
  			this.accion = 'agregar';
  		}

  		// Mostrar modal
  		$('#modalCategoria').modal('show');
  	}

  	private buildForm(){
		this.forma = new FormGroup({
			'nombre': new FormControl('', Validators.required)
		});
  	}

  	resetForm(){
  		this.forma.reset();
  	}
}
