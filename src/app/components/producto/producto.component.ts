import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Tabla } from '../../clases/Tabla';
import { Producto, Marca, Categoria, Proveedor } from '../../interfaces/interfaces';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';

import Swal from 'sweetalert2';

// Servicios
import { ProductosService } from '../../services/productos.service';
import { MarcasService } from '../../services/marcas.service';
import { CategoriasService } from '../../services/categorias.service';
import { ProveedoresService } from '../../services/proveedores.service';

// Para usar jQuery
declare var $: any;

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styles: []
})
export class ProductoComponent implements OnInit, OnDestroy {
	@ViewChild(DataTableDirective, {static: false}) dtElement: DataTableDirective;
	
	productos: Producto[] = [];
	marcas: Marca[] = [];
	categorias: Categoria[] = [];
	proveedores: Proveedor[] = [];

	tabla = new Tabla();
	forma: FormGroup;
	accion: string;
	productoEditar: Producto;

	constructor(private _productosServices: ProductosService,
				private _marcasServices: MarcasService,
				private _categoriasServices: CategoriasService,
				private _proveedoresServices: ProveedoresService){

		this._productosServices.getAll().subscribe( (productos: Producto[]) => {
			this.productos = productos;
			this.tabla.dtTrigger.next();
		});

		this._marcasServices.getAll().subscribe( (marcas: Marca[]) => {
			this.marcas = marcas;
		});

		this._categoriasServices.getAll().subscribe( (categorias: Categoria[]) => {
			this.categorias = categorias;
		});

		this._proveedoresServices.getAll().subscribe( (proveedores: Proveedor[]) => {
			this.proveedores = proveedores;
		});
	}

	ngOnInit() {
		this.buildForm();
	}

	ngOnDestroy(){
		// Do not forget to unsubscribe the event
	    this.tabla.dtTrigger.unsubscribe();
	}

	agregarProducto(){
		// Guardar datos
		var producto = {
			codigo: this.forma.value.codigo,
			nombre: this.forma.value.nombre.toUpperCase(),
			marca: this.forma.value.marca,
			categoria: this.forma.value.categoria,
			proveedor: this.forma.value.proveedor,
			precio_proveedor: this.forma.value.precio_proveedor,
			precio_venta: this.forma.value.precio_venta,
			cantidad: this.forma.value.cantidad
		}

		this._productosServices.agregarProducto(producto).subscribe( (resp: any) => {
			if(resp.ok){
				// Agregar a la lista de productos
  				this.productos.push(resp.detalle);

  				// Ordenar productos por nombre en asc 
  				this.productos.sort((unProducto, otroProducto) => unProducto.nombre.localeCompare(otroProducto.nombre));

				// Ocultar modal
  				$('#modalProducto').modal('hide');

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `${ resp.mensaje }`, 'success');

  				// Reset formulario
  				this.resetForm();
			} else {
				// Ocultar modal
  				$('#modalProducto').modal('hide');

  				// Mostrar alerta
  				Swal.fire('Oops!', `${ resp.mensaje }`, 'error');

  				// Reset formulario
  				this.resetForm();
			}
		});
	}

	editarProducto(){
		// Guardar datos
		var producto = {
			nombre: this.forma.value.nombre.toUpperCase(),
			marca: this.forma.value.marca,
			categoria: this.forma.value.categoria,
			proveedor: this.forma.value.proveedor,
			precio_proveedor: this.forma.value.precio_proveedor,
			precio_venta: this.forma.value.precio_venta,
			cantidad: this.forma.value.cantidad
		}

  		this._productosServices.editarProducto(this.productoEditar.id, producto).subscribe( (resp: any) => {
  			if(resp.ok){
  				// Actualizar datos en lista de productos 
  				const index = this.productos.findIndex(producto => producto.id == this.productoEditar.id);
  				this.productos[index].marca_id = resp.detalle.marca_id;
  				this.productos[index].categoria_id = resp.detalle.categoria_id;
  				this.productos[index].proveedor_id = resp.detalle.proveedor_id;
  				this.productos[index].codigo = resp.detalle.codigo;
  				this.productos[index].nombre = resp.detalle.nombre;
  				this.productos[index].marca = resp.detalle.marca;
  				this.productos[index].categoria = resp.detalle.categoria;
  				this.productos[index].proveedor = resp.detalle.proveedor;
  				this.productos[index].precio_proveedor = resp.detalle.precio_proveedor;
  				this.productos[index].precio_venta = resp.detalle.precio_venta;
  				this.productos[index].cantidad = resp.detalle.cantidad;

  				// Ordenar productos por nombre en asc 
  				this.productos.sort((unProducto, otroProducto) => unProducto.nombre.localeCompare(otroProducto.nombre));

  				// Ocultar modal
  				$('#modalProducto').modal('hide');

  				// Renderizar tabla
  				this.tabla.rerender(this.dtElement);

  				// Mostrar alerta
  				Swal.fire('', `${ resp.mensaje }`, 'success');

  				// Reset formulario
  				this.resetForm();
  			} else {
  				// Ocultar modal
  				$('#modalProducto').modal('hide');

  				// Mostrar alerta
  				Swal.fire('Oops!', `${ resp.mensaje }`, 'error');

  				// Reset formulario
  				this.resetForm();
  			}
  		});
  	}

	eliminarProducto(producto: Producto, i: number){
		this._productosServices.eliminarProducto(producto.id).subscribe( (resp: any) => {
			// Eliminar de la lista de productos
			this.productos.splice(i, 1);

			// Renderizar tabla
			this.tabla.rerender(this.dtElement);

			// Mostrar alerta
			Swal.fire('', `El producto ${ resp.nombre } con marca ${ resp.marca } ha sido eliminado correctamente.`, 'success');
		});
	}

	onSubmit(){
		if(this.accion == 'agregar'){
  			this.agregarProducto();
  		} else {
  			this.editarProducto();
  		}
	}

	showModal(producto?: Producto){
		if(producto){
  			this.accion = 'editar';

  			this.forma.controls['codigo'].disable();
 			this.forma.controls['codigo'].setValue(producto.codigo);
  			this.forma.controls['nombre'].setValue(producto.nombre.toUpperCase());
  			this.forma.controls['marca'].setValue(producto.marca_id);
  			this.forma.controls['categoria'].setValue(producto.categoria_id);
  			this.forma.controls['proveedor'].setValue(producto.proveedor_id);
  			this.forma.controls['precio_proveedor'].setValue(producto.precio_proveedor);
  			this.forma.controls['precio_venta'].setValue(producto.precio_venta);
  			this.forma.controls['cantidad'].setValue(producto.cantidad);
  			this.productoEditar = producto;
  		} else {
  			this.accion = 'agregar';
  			this.forma.controls['codigo'].enable();
  		}
  		
  		// Mostrar modal
  		$('#modalProducto').modal('show');
  	} 


	private buildForm(){
		this.forma = new FormGroup({
			'codigo': new FormControl('', Validators.required),
			'nombre': new FormControl('', Validators.required),
			'marca': new FormControl(0, Validators.min(1)),
			'categoria': new FormControl(0, Validators.min(1)),
			'proveedor': new FormControl(0, Validators.min(1)),
			'precio_proveedor': new FormControl('', [Validators.required, Validators.min(1)]),
			'precio_venta': new FormControl('', [Validators.required, Validators.min(1)]),
			'cantidad': new FormControl('', [Validators.required, Validators.min(0)])
		});
  	}

  	resetForm(){
        this.forma.reset();
        this.forma.controls['marca'].setValue(0);
        this.forma.controls['categoria'].setValue(0);
        this.forma.controls['proveedor'].setValue(0);
  	}

}
